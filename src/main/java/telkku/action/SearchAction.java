package telkku.action;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Transaction;

import telkku.db.CloseableSession;
import telkku.db.HibernateUtil;
import telkku.other.Helper;
import telkku.other.Program;
import telkku.other.ProgramTimeComparator;

public class SearchAction extends TelkkuAction {
	private static final long serialVersionUID = -397236277479401859L;
	private static final Logger logger = Logger.getLogger(SearchAction.class);
	private List<Program> programList = new ArrayList<>();
	private static Comparator<Program> comparator = new ProgramTimeComparator();
	private final Calendar today = Calendar.getInstance();
	private String searchString;

	@SuppressWarnings("unchecked")
	@Override
	public String execute() {
		logger.debug(searchString);

		if (searchString != null && !searchString.isEmpty()) {
			try (CloseableSession session = new CloseableSession(HibernateUtil.getSessionFactory().getCurrentSession())) {
				Transaction tx;

				logger.debug("Searching programs...");

				tx = session.delegate().beginTransaction();
				Date startDate = today.getTime();
				Date endDate = Helper.addDaysToDate(startDate, 7);

				String queryString = "FROM Program WHERE programName LIKE :name AND programStartTime BETWEEN :startTime AND :endTime";

				Query query = session.delegate().createQuery(queryString);

				query.setParameter("name", searchString);
				query.setParameter("startTime", startDate);
				query.setParameter("endTime", endDate);

				programList = query.list();

				tx.commit();

				logger.debug(programList.size() + " programs found.");
				addActionMessage(programList.size() + " programs found.");

				Collections.sort(programList, comparator);
			} catch (HibernateException e) {
				logger.debug(e.getMessage());
				addActionMessage(e.getMessage());
			}
		}

		return SUCCESS;
	}

	public List<Program> getProgramList() {
		return programList;
	}

	public String getSearchString() {
		return searchString;
	}

	public void setSearchString(String searchString) {
		this.searchString = searchString;
	}
}
