package telkku.action;

import java.util.List;

import telkku.other.Program;

public class MainAction extends TelkkuAction {
	private static final long serialVersionUID = -397236277179401859L;

	@Override
	public String execute() {
		return SUCCESS;
	}

	public List<Program> getFavouritePrograms() {
		return Tasks.getFavouriteProgramList();
	}

	public List<Program> getMovies() {
		return Tasks.getMovieList();
	}
}
