package telkku.action;

import java.text.ParseException;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Transaction;

import telkku.db.CloseableSession;
import telkku.db.HibernateUtil;
import telkku.other.Channel;
import telkku.other.Helper;
import telkku.other.Program;

public class AllChannelsAction extends TelkkuAction {
	private static final long serialVersionUID = -397236277079401859L;
	private static final Logger logger = Logger.getLogger(AllChannelsAction.class);
	private String date;
	private Date dateDate;
	private List<List<List<Program>>> blockList = new ArrayList<>();
	private List<List<Program>> programsList = new ArrayList<>();
	private List<Channel> channelListFiltered = new ArrayList<>();
	private List<Long> channelIds = new ArrayList<>();
	private Map<Boolean, String> yesNoList;
	private Boolean descriptions;
	private final List<Integer> favouriteProgramIds = new ArrayList<>();

	public AllChannelsAction() {
		super();
		this.yesNoList = new HashMap<Boolean, String>();
		yesNoList.put(true, "Kyllä");
		yesNoList.put(false, "Ei");
	}

	@Override
	public String execute() {
		super.execute();
		prepareDate();
		formTimeBlocks(dateDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
		prepareChannels();
		fetchFavouritePrograms();

		logger.debug("Showing programs for date: " + date);

		fetchPrograms();

		if (userSession.get("descriptions") == null) {
			userSession.put("descriptions", false);
		} else {
			if (descriptions != null) {
				userSession.put("descriptions", descriptions);
				logger.debug("Changed 'show program descriptions' to: " + descriptions);
				addActionMessage("Property 'show program descriptions' is set to " + descriptions);
			}
		}

		return SUCCESS;
	}

	private void fetchPrograms() {
		try (CloseableSession session = new CloseableSession(HibernateUtil.getSessionFactory().getCurrentSession())) {
			Transaction tx = session.delegate().beginTransaction();
			for (int i = 0; i < timeBlocks.size() - 1; i++) {
				programsList.clear();
				for (Channel tmpChannel : channelListFiltered) {
					String sql = "from Program WHERE programStartTime >= :date1 AND programStartTime < :date2 AND channel = " + tmpChannel.getChannelId()
							+ " ORDER BY programStartTime";
					Query query = session.delegate().createQuery(sql);
					query.setString("date1", Helper.df2.format(timeBlocks.get(i)));
					query.setString("date2", Helper.df2.format(timeBlocks.get(i + 1)));

					@SuppressWarnings("unchecked")
					List<Program> tmpList = query.list();
					programsList.add(tmpList);
				}
				blockList.add(new ArrayList<>(programsList));
			}
			tx.commit();
		} catch (HibernateException e) {
			logger.debug(e.getMessage());
			addActionMessage(e.getMessage());
		}
	}

	private void fetchFavouritePrograms() {
		Date startDate;

		startDate = dateDate;

		Date endDate = Helper.addDaysToDate(startDate, 1);

		try (CloseableSession session = new CloseableSession(HibernateUtil.getSessionFactory().getCurrentSession())) {
			Transaction tx = session.delegate().beginTransaction();

			for (String tmpProgram : getFavouriteProgramList()) {
				String sql = "SELECT programId FROM Program WHERE programName LIKE :programName AND programStartTime BETWEEN :date1 AND :date2";

				Query query = session.delegate().createSQLQuery(sql);
				query.setParameter("programName", tmpProgram);
				query.setParameter("date1", startDate);
				query.setParameter("date2", endDate);

				@SuppressWarnings("unchecked")
				List<Integer> tmpProgramList = query.list();

				if (tmpProgramList.size() > 0) {
					favouriteProgramIds.addAll(tmpProgramList);
				}
			}

			tx.commit();
		} catch (HibernateException e) {
			logger.debug(e.getMessage());
			addActionMessage(e.getMessage());
		}
	}

	@SuppressWarnings("unchecked")
	private void prepareChannels() {
		if (channelIds == null || channelIds.isEmpty()) {
			channelIds = (List<Long>) userSession.get("channelIds");
		}

		if (channelIds == null || channelIds.isEmpty()) {
			channelListFiltered = getChannelList();
			channelIds = new ArrayList<>();
			for (Channel tmpChannel : channelListFiltered) {
				channelIds.add(tmpChannel.getChannelId());
			}
		} else {
			for (Channel tmpChannel : getChannelList()) {
				for (Long channelId : channelIds) {
					if (tmpChannel.getChannelId().equals(channelId)) {
						channelListFiltered.add(tmpChannel);
						break;
					}
				}
			}
		}

		userSession.put("channelIds", channelIds);
	}

	private void prepareDate() {
		// Tarkistetaan tieto ensin sessiosta ja jos ei löydy otetaan nykyinen
		// päivä.
		if (date == null) {
			date = (String) userSession.get("selectedDate");
			if (date == null) {
				date = Helper.df1.format(new Date());
			}
		}

		userSession.put("selectedDate", date);

		try {
			dateDate = Helper.df1.parse(date);
		} catch (ParseException e) {
			addActionError(e.getMessage());
		}
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public List<List<Program>> getProgramsList() {
		return programsList;
	}

	public void setProgramsList(List<List<Program>> programsList) {
		this.programsList = programsList;
	}

	public List<Long> getChannelIds() {
		return channelIds;
	}

	public void setChannelIds(List<Long> channelIds) {
		this.channelIds = channelIds;
	}

	public List<Channel> getChannelListFiltered() {
		return channelListFiltered;
	}

	public void setChannelListFiltered(List<Channel> channelListFiltered) {
		this.channelListFiltered = channelListFiltered;
	}

	public List<List<List<Program>>> getBlockList() {
		return blockList;
	}

	public void setBlockList(List<List<List<Program>>> blockList) {
		this.blockList = blockList;
	}

	public Map<Boolean, String> getYesNoList() {
		return yesNoList;
	}

	public void setYesNoList(Map<Boolean, String> yesNoList) {
		this.yesNoList = yesNoList;
	}

	public Boolean isDescriptions() {
		return descriptions;
	}

	public void setDescriptions(Boolean descriptions) {
		this.descriptions = descriptions;
	}

	public List<Integer> getFavouriteProgramIds() {
		return favouriteProgramIds;
	}
}
