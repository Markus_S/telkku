package telkku.action;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.hibernate.Transaction;
import org.hibernate.search.FullTextSession;
import org.hibernate.search.Search;

import telkku.db.CloseableSession;
import telkku.db.HibernateUtil;
import telkku.other.Channel;
import telkku.other.FavouriteProgram;

import com.opensymphony.xwork2.ActionSupport;

public class Bootstrap extends ActionSupport {
	private static final long serialVersionUID = -397236277079401759L;
	private static Logger logger = Logger.getLogger(Bootstrap.class);
	private static List<String> channels = new ArrayList<String>();

	public Bootstrap() {
		//Load channel names and logos from bootstrap-file
		try (InputStream is = Bootstrap.class.getResourceAsStream("Bootstrap.xml")) {
			Properties prop = new Properties();
			prop.loadFromXML(is);

			List<String> keys = new ArrayList<String>();
			for (String key : prop.stringPropertyNames()) {
				keys.add(key);
			}
			Collections.sort(keys);

			for (Object key : keys) {
				channels.add((String) prop.get(key));
			}
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	@Override
	public String execute() throws Exception {

		// CREATE USER 'telkku'@'localhost' IDENTIFIED BY '***';
		// GRANT USAGE ON *.* TO 'telkku'@'localhost' IDENTIFIED BY '***' WITH
		// MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0
		// MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;
		//
		// CREATE DATABASE IF NOT EXISTS `telkku`;
		// GRANT ALL PRIVILEGES ON `telkku`.* TO 'telkku'@'localhost';

		logger.debug("Bootstrapping the system.");

		try (CloseableSession session = new CloseableSession(HibernateUtil.getSessionFactory().getCurrentSession())) {
			Transaction tx = session.delegate().beginTransaction();

			// Jos kanavalista on tyhjä, luodaan kanavat tietokantaan
			if (session.delegate().createQuery("from Channel").list().isEmpty()) {
				logger.debug("Creating channels ....");
				String[] splitChannel;

				for (String channel : channels) {
					splitChannel = channel.split(";");
					Channel tmpChannel = new Channel();
					tmpChannel.setChannelName(splitChannel[0]);
					tmpChannel.setChannelLogo("images/" + splitChannel[1]);

					session.delegate().save(tmpChannel);
				}

				FavouriteProgram suosikki = new FavouriteProgram();
				suosikki.setFavouriteProgramName("Uutiset%");
				session.delegate().save(suosikki);
				logger.debug(channels.size() + " channels created.");
			}

			tx.commit();
		}
		
		updateIndex();

		return SUCCESS;
	}
	
	private void updateIndex() {
		try (CloseableSession session = new CloseableSession(HibernateUtil.getSessionFactory().getCurrentSession())) {
			logger.debug("Indexing started...");
			
	        FullTextSession fullTextSession = Search.getFullTextSession(session.delegate());
	        fullTextSession.createIndexer().startAndWait();
	        
	        fullTextSession.getSearchFactory().optimize();
	        
	        fullTextSession.close();
	        
	        logger.debug("Indexing stopped");
		} catch (InterruptedException e) {
			e.printStackTrace();
		}		
	}
}
