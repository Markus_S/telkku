package telkku.action;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.BooleanClause.Occur;
import org.hibernate.HibernateException;
import org.hibernate.Transaction;
import org.hibernate.search.FullTextSession;
import org.hibernate.search.Search;
import org.hibernate.search.query.dsl.QueryBuilder;

import telkku.db.CloseableSession;
import telkku.db.HibernateUtil;
import telkku.other.Channel;
import telkku.other.Helper;
import telkku.other.ParseModel;
import telkku.other.Program;
import telkku.other.ProgramTimeComparator;

import com.opensymphony.xwork2.ActionSupport;

public class Tasks extends ActionSupport {
	private static final long serialVersionUID = -397246277179401859L;
	private static final Logger logger = Logger.getLogger(Tasks.class);
	private static Calendar today = Calendar.getInstance();
	private static List<Program> favouriteProgramList = new ArrayList<>();
	private static List<Program> movieList = new ArrayList<>();
	private static List<Channel> channelList = new ArrayList<>();
	private static List<String> favouriteProgramNameList = new ArrayList<>();
	private static Comparator<Program> comparator = new ProgramTimeComparator();
	private String command;
	private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
	private static boolean maintenanceStarted;
	private Boolean debugMode = Boolean.TRUE;
	
	public Tasks() {
		if (channelList.size() == 0) {
			refreshChannelList();
		}

		if (favouriteProgramList.size() == 0) {
			refreshFavouriteProgramList();
		}
		
		if (!maintenanceStarted) {
			updateIndex();
			
			logger.info("Starting maintenance...");
			maintenanceStarted = true;
			
			if (!debugMode) {
				doMaintenance(Helper.getMaintenanceStartDate());	
			} else {
				doMaintenance(LocalDateTime.now());
			}
		}
	}
	
	public String execute() {
		if (command != null && !command.isEmpty()) {
			if (command.equals("updateMovies")) {
				checkMovies();
			} else if (command.equals("updateFavourites")) {
				checkFavouritePrograms();
			} else if (command.equals("updatePrograms")) {
				checkUpdates();
			} else if (command.equals("refreshChannels")) {
				refreshChannelList();
			} else if (command.equals("refreshFavourites")) {
				refreshFavouriteProgramList();
			} else if (command.equals("updateIndex")) {
				updateIndex();
			}
		}
		
		return SUCCESS;
	}

	private void checkMovies() {
		try (CloseableSession session = new CloseableSession(HibernateUtil.getSessionFactory().getCurrentSession())) {
			Transaction tx;

			logger.debug("Checking movies...");
			
			movieList.clear();

			tx = session.delegate().beginTransaction();
			
			FullTextSession fullTextSession = Search.getFullTextSession(session.delegate());
			
			QueryBuilder programQB = fullTextSession.getSearchFactory().buildQueryBuilder().forEntity(Program.class).get();
			
			Query nameQuery = programQB.keyword().onField("programName").matching("dokumenttielokuva elokuva leffa").createQuery();
			Query descriptionQuery = programQB.keyword().onField("programDescription").matching("komedia trilleri draama elokuva ohjaaja ohjaus pääosissa O: N: P: esittää").createQuery();
			Query seriesQuery = programQB.keyword().onField("programDescription").matching("'. kausi' realitysarja asiaohjelma TV-sarja dokumenttisarja sketsisarja").createQuery();
			Query startTimeQuery = programQB.range().onField("programStartTime").above(today.getTime()).createQuery();
			
			Query luceneQuery = programQB
					.bool()
						.should(nameQuery) //TODO Lisää aikaperustainen hakuperuste
						.must(seriesQuery).not()
						.must(descriptionQuery)
						.must(startTimeQuery)
					.createQuery();

			@SuppressWarnings("unchecked")
			List<Program> tmpProgramList = fullTextSession.createFullTextQuery(luceneQuery, Program.class).list();

			tx.commit();

			if (tmpProgramList.size() > 0) {
				for (Program tmpProgram : tmpProgramList) {
					tmpProgram.setProgramName(tmpProgram.getProgramName().replaceFirst("Elokuva: ", ""));
					tmpProgram.setProgramName(tmpProgram.getProgramName().replaceFirst("Kino: ", ""));
					tmpProgram.setProgramName(tmpProgram.getProgramName().replaceFirst("Kesäleffa: ", ""));
					movieList.add(tmpProgram);
				}
			}

			logger.debug(movieList.size() + " movies found.");
			addActionMessage(movieList.size() + " movies found.");

			Collections.sort(movieList, comparator);
		} catch (HibernateException e) {
			logger.debug(e.getMessage());
			addActionMessage(e.getMessage());
		}
	}
	
	private void checkFavouritePrograms() {
		try (CloseableSession session = new CloseableSession(HibernateUtil.getSessionFactory().getCurrentSession())) {
			Transaction tx;

			logger.debug("Checking favourite programs...");

			tx = session.delegate().beginTransaction();
			FullTextSession fullTextSession = Search.getFullTextSession(session.delegate());
			QueryBuilder programQB = fullTextSession.getSearchFactory().buildQueryBuilder().forEntity(Program.class).get();
			
			Query startTimeQuery = programQB.range().onField("programStartTime").above(today.getTime()).createQuery();

			favouriteProgramList.clear();
			
			for (String programName : favouriteProgramNameList) {
				String[] tokenized = removeStopWords(programName).split("\\s");
				BooleanQuery bQuery = new BooleanQuery();
				
				bQuery.add(startTimeQuery, Occur.MUST);
				
				for(int i=0;i<tokenized.length;i++){
					Query query = programQB.keyword().onField("programName").matching(tokenized[i]).createQuery();
					bQuery.add(query, BooleanClause.Occur.MUST);
				}

				@SuppressWarnings("unchecked")
				List<Program> tmpProgramList = fullTextSession.createFullTextQuery(bQuery, Program.class).list();

				if (tmpProgramList.size() > 0) {
					favouriteProgramList.addAll(tmpProgramList);
				}
			}

			tx.commit();

			logger.debug(favouriteProgramList.size() + " favourite programs found.");
			addActionMessage(favouriteProgramList.size() + " favourite programs found.");

			Collections.sort(favouriteProgramList, comparator);
		} catch (HibernateException e) {
			logger.debug(e.getMessage());
			addActionMessage(e.getMessage());
		}
	}
	
	private void checkUpdates() {
		Date maxDate;
		Date endDate;
		String channelName;

		logger.debug("Checking updates for programs...");
		
		for (Channel channel : channelList) {
			// maxDate = today.getTime();
			maxDate = Helper.addDaysToDate(today.getTime(), -30);
			channelName = channel.getChannelName();

			Date dbMaxDate = getDbMaxDate(channel.getChannelId());

			if (dbMaxDate != null) {
				maxDate = dbMaxDate;
			}

			// Reset time to 00:00
			maxDate = Helper.stripTime(maxDate);
			endDate = Helper.stripTime(today.getTime());
			endDate = Helper.addDaysToDate(endDate, 7);

			if (endDate.after(maxDate) || endDate.equals(maxDate)) {
				getPrograms(maxDate, endDate, channelName);
			}
		}

		logger.debug("Program updates checked.");
	}

	private void getPrograms(Date maxDate, Date endDate, String channelName) {
		List<Date> days = new ArrayList<>();
		Date compareDay = new Date(maxDate.getTime());
		Date tmpCompareDay;

		do {
			tmpCompareDay = (Date) compareDay.clone();
			compareDay = Helper.addDaysToDate(compareDay, 1);
			days.add((Date) tmpCompareDay.clone());
		} while (endDate.after(tmpCompareDay));

		logger.debug("Need to scrape programs for " + channelName + " for " + days.size() + " days.");

		ParseModel pm = new ParseModel();

		for (Date day : days) {
			pm.parsePrograms(channelName, day);
		}

		addActionMessage("Scraped new programs to channel " + channelName + " for worth of " + days.size() + " days");
	}

	private Date getDbMaxDate(Long channelId) {
		Transaction tx;
		Date dbMaxDate = null;
		String sqlQuery = "SELECT MAX(programStartTime) AS 'date' FROM Program WHERE channelId = " + channelId;

		try (CloseableSession session = new CloseableSession(HibernateUtil.getSessionFactory().getCurrentSession())) {
			tx = session.delegate().beginTransaction();
			dbMaxDate = (Date) session.delegate().createSQLQuery(sqlQuery).uniqueResult();
			tx.commit();
		} catch (HibernateException e) {
			logger.debug(e.getMessage());
			addActionMessage(e.getMessage());
		}

		return dbMaxDate;
	}
	
	@SuppressWarnings("unchecked")
	private void refreshChannelList() {
		try (CloseableSession session = new CloseableSession(HibernateUtil.getSessionFactory().getCurrentSession())) {
			Transaction tx = session.delegate().beginTransaction();
			channelList = session.delegate().createQuery("FROM Channel").list();
			tx.commit();
		} catch (HibernateException e) {
			logger.debug(e.getMessage());
			addActionMessage(e.getMessage());
		}
	}

	@SuppressWarnings("unchecked")
	private void refreshFavouriteProgramList() {
		try (CloseableSession session = new CloseableSession(HibernateUtil.getSessionFactory().getCurrentSession())) {
			Transaction tx = session.delegate().beginTransaction();
			favouriteProgramNameList = session.delegate().createSQLQuery("SELECT favouriteProgramName FROM FavouriteProgram").list();
			tx.commit();
		} catch (HibernateException e) {
			logger.debug(e.getMessage());
			addActionMessage(e.getMessage());
		}
	}
	
	public void doMaintenance(LocalDateTime startDate) {
		final long repeatInterval;
		
		final Runnable maintenanceRunnable = () -> {
			today = Calendar.getInstance(); //Päivitä nykyinen päivä
			checkUpdates();
			checkMovies();
			checkFavouritePrograms();
		};
		
		if (debugMode) {
			repeatInterval = 300; //seconds
		} else {
			repeatInterval = TimeUnit.DAYS.toSeconds(1);
		}
		
		//Aloita parametrissa startDate määritettynä aikana ja toista X sekunnin välein
		final long initialDelay = LocalDateTime.now().until(startDate, ChronoUnit.SECONDS);
		final ScheduledFuture<?> maintainHandle = scheduler.scheduleAtFixedRate(maintenanceRunnable, initialDelay, repeatInterval, TimeUnit.SECONDS);

		Runnable scheduledShutdownRunnable = () -> {
			maintainHandle.cancel(true);
			scheduler.shutdown();
			maintenanceStarted = false;
		};
		
		scheduler.schedule(scheduledShutdownRunnable, 600, TimeUnit.DAYS); //Aja tehtävää 600 päivän ajan
	}
	
	private void updateIndex() {
		try (CloseableSession session = new CloseableSession(HibernateUtil.getSessionFactory().getCurrentSession())) {
			logger.debug("Indexing started...");
			
	        FullTextSession fullTextSession = Search.getFullTextSession(session.delegate());
	        fullTextSession.createIndexer().startAndWait();
	        
	        fullTextSession.getSearchFactory().optimize();
	        
	        fullTextSession.close();
	        
	        logger.debug("Indexing stopped");
		} catch (InterruptedException e) {
			e.printStackTrace();
		}		
	}
	
	private static String removeStopWords(String searchText) {
		searchText = searchText.replaceAll(" and ", "");
		searchText = searchText.replaceAll(" of ", "");
		return searchText;
	}
	
	public static List<Program> getFavouriteProgramList() {
		return favouriteProgramList;
	}

	public static List<Program> getMovieList() {
		return movieList;
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}
}
