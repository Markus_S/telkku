package telkku.action;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.SessionAware;
import org.hibernate.HibernateException;
import org.hibernate.Transaction;

import telkku.db.CloseableSession;
import telkku.db.HibernateUtil;
import telkku.other.Channel;

import com.opensymphony.xwork2.ActionSupport;

public class TelkkuAction extends ActionSupport implements SessionAware {
	private static final long serialVersionUID = -397236277479201859L;
	private static final Logger logger = Logger.getLogger(TelkkuAction.class);
	private static List<Channel> channelList = new ArrayList<>();
	private static List<String> favouriteProgramList = new ArrayList<>();
	protected List<Date> timeBlocks = new ArrayList<>();
	protected Map<String, Object> userSession;
	private boolean refresh;

	public TelkkuAction() {
		if (channelList.size() == 0) {
			refreshChannelList();
		}

		if (favouriteProgramList.size() == 0) {
			refreshFavouriteProgramList();
		}
	}

	@Override
	public String execute() {
		if (refresh) {
			refreshChannelList();
			refreshFavouriteProgramList();
		}

		return SUCCESS;
	}

	@SuppressWarnings("unchecked")
	protected void refreshChannelList() {
		try (CloseableSession session = new CloseableSession(HibernateUtil.getSessionFactory().getCurrentSession())) {
			Transaction tx = session.delegate().beginTransaction();
			channelList = session.delegate().createQuery("FROM Channel").list();
			tx.commit();
		} catch (HibernateException e) {
			logger.debug(e.getMessage());
			addActionMessage(e.getMessage());
		}
	}

	@SuppressWarnings("unchecked")
	protected void refreshFavouriteProgramList() {
		try (CloseableSession session = new CloseableSession(HibernateUtil.getSessionFactory().getCurrentSession())) {
			Transaction tx = session.delegate().beginTransaction();
			favouriteProgramList = session.delegate().createSQLQuery("SELECT favouriteProgramName FROM FavouriteProgram").list();
			tx.commit();
		} catch (HibernateException e) {
			logger.debug(e.getMessage());
			addActionMessage(e.getMessage());
		}
	}

	protected void formTimeBlocks(LocalDate date) {
		LocalDateTime tmpDateTime;

		timeBlocks.clear();

		for (int i = 1; i < 6; i++) {
			tmpDateTime = LocalDateTime.of(date, LocalTime.MIN).plusHours(i * 6);
			timeBlocks.add(Date.from(tmpDateTime.atZone(ZoneId.systemDefault()).toInstant()));
		}
	}

	@Override
	public void setSession(Map<String, Object> session) {
		userSession = session;
	}

	public static List<Channel> getChannelList() {
		return channelList;
	}

	public static List<String> getFavouriteProgramList() {
		return favouriteProgramList;
	}

	public void setRefresh(boolean refresh) {
		this.refresh = refresh;
	}
}
