package telkku.action;

import java.text.ParseException;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Transaction;

import telkku.db.CloseableSession;
import telkku.db.HibernateUtil;
import telkku.other.Channel;
import telkku.other.Helper;
import telkku.other.Program;

public class SingleChannelAction extends TelkkuAction {
	private static final long serialVersionUID = -397236277079401859L;
	private static final Logger logger = Logger.getLogger(SingleChannelAction.class);
	private String date;
	private Date dateDate;
	private List<List<List<Program>>> blockList = new ArrayList<>();
	private List<List<Program>> programsList = new ArrayList<>();
	private String singleChannel;
	private final List<Integer> favouriteProgramIds = new ArrayList<>();
	private Channel channel;

	public SingleChannelAction() {
		super();
	}

	@Override
	public String execute() {
		super.execute();
		prepareDate();

		formTimeBlocks(dateDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate());

		prepareChannel();

		fetchFavouritePrograms();

		logger.debug("Showing programs for date: " + date + " and channel: " + channel.getChannelName());
		fetchPrograms();

		return SUCCESS;
	}

	private void prepareChannel() {
		// Tarkistetaan tieto ensin sessiosta ja jos ei löydy valitaan
		// ensimmäinen kanava listasta.
		if (singleChannel == null || singleChannel.isEmpty()) {
			singleChannel = (String) userSession.get("singleChannel");
			if (singleChannel == null || singleChannel.isEmpty()) {
				singleChannel = getChannelList().get(0).getChannelName();
			}
		}

		userSession.put("singleChannel", singleChannel);

		for (Channel ch : getChannelList()) {
			if (ch.getChannelName().equals(singleChannel)) {
				channel = ch;
				break;
			}
		}
	}

	private void prepareDate() {
		// Tarkistetaan tieto ensin sessiosta ja jos ei löydy otetaan nykyinen
		// päivä.
		if (date == null) {
			date = (String) userSession.get("selectedDate");
			if (date == null) {
				date = Helper.df1.format(new Date());
			}
		}

		userSession.put("selectedDate", date);

		try {
			dateDate = Helper.df1.parse(date);
		} catch (ParseException e) {
			addActionError(e.getMessage());
		}
	}

	private void fetchPrograms() {
		try (CloseableSession session = new CloseableSession(HibernateUtil.getSessionFactory().getCurrentSession())) {
			Transaction tx = session.delegate().beginTransaction();

			for (int i = 0; i < timeBlocks.size() - 1; i++) {
				programsList.clear();
				String sql = "from Program WHERE programStartTime >= :date1 AND programStartTime < :date2 AND channel = " + channel.getChannelId()
						+ " ORDER BY programStartTime";
				Query query = session.delegate().createQuery(sql);
				query.setParameter("date1", timeBlocks.get(i));
				query.setParameter("date2", timeBlocks.get(i + 1));

				@SuppressWarnings("unchecked")
				List<Program> tmpList = query.list();
				programsList.add(tmpList);
				blockList.add(new ArrayList<>(programsList));
			}

			tx.commit();
		} catch (HibernateException e) {
			logger.debug(e.getMessage());
			addActionMessage(e.getMessage());
		}
	}

	private void fetchFavouritePrograms() {
		Date startDate;

		startDate = dateDate;

		Date endDate = Helper.addDaysToDate(startDate, 1);

		try (CloseableSession session = new CloseableSession(HibernateUtil.getSessionFactory().getCurrentSession())) {
			Transaction tx = session.delegate().beginTransaction();

			for (String tmpProgram : getFavouriteProgramList()) {
				String sql = "SELECT programId FROM Program WHERE programName LIKE :programName AND programStartTime BETWEEN :date1 AND :date2 AND channelId = "
						+ channel.getChannelId();

				Query query = session.delegate().createSQLQuery(sql);
				query.setParameter("programName", tmpProgram);
				query.setParameter("date1", startDate);
				query.setParameter("date2", endDate);

				@SuppressWarnings("unchecked")
				List<Integer> tmpProgramList = query.list();

				if (tmpProgramList.size() > 0) {
					favouriteProgramIds.addAll(tmpProgramList);
				}
			}

			tx.commit();
		} catch (HibernateException e) {
			logger.debug(e.getMessage());
			addActionMessage(e.getMessage());
		}
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public List<List<Program>> getProgramsList() {
		return programsList;
	}

	public void setProgramsList(List<List<Program>> programsList) {
		this.programsList = programsList;
	}

	public String getSingleChannel() {
		return singleChannel;
	}

	public void setSingleChannel(String singleChannel) {
		this.singleChannel = singleChannel;
	}

	public List<List<List<Program>>> getBlockList() {
		return blockList;
	}

	public void setBlockList(List<List<List<Program>>> blockList) {
		this.blockList = blockList;
	}

	public List<Integer> getFavouriteProgramIds() {
		return favouriteProgramIds;
	}

	public Channel getChannel() {
		return channel;
	}
}
