package telkku.db;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;


/**
 * Startup Hibernate and provide access to the singleton SessionFactory.
 */
 
public class HibernateUtil {

	private static final SessionFactory sessionFactory = buildSessionFactory();
	private static final Logger logger = Logger.getLogger(HibernateUtil.class);
	
	private static SessionFactory buildSessionFactory() {
//		return new Configuration().configure().buildSessionFactory();
        try {
            // Create the SessionFactory from hibernate.cfg.xml
        	Configuration configuration = new Configuration().configure();
        	ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
        										.applySettings(configuration.getProperties()).build();
        	return configuration.buildSessionFactory(serviceRegistry);
        } catch (HibernateException ex) {
            // Make sure you log the exception, as it might be swallowed
            logger.error("Initial SessionFactory creation failed." + ex);
            System.out.println("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static SessionFactory getSessionFactory() {
    	if (sessionFactory == null || sessionFactory.isClosed()) {
    		buildSessionFactory();
    	}
    	
        return sessionFactory;
    }
}
