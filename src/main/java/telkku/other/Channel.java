package telkku.other;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table
public class Channel {
	
	@Id
	@GeneratedValue
	private Long channelId;
	private String channelLogo;
	private String channelName;

	public Channel(){	}
	
	public Long getChannelId() {
		return channelId;
	}
	
	public void setChannelId(Long channelId) {
		this.channelId = channelId;
	}

	public String getChannelLogo() {
		return channelLogo;
	}

	public void setChannelLogo(String channelLogo) {
		this.channelLogo = channelLogo;
	}

	public String getChannelName() {
		return channelName;
	}

	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}

	@Override
	public String toString() {
		return "Channel [channelId=" + channelId + ", channelLogo="
				+ channelLogo + ", channelName=" + channelName + "]";
	}	
}
