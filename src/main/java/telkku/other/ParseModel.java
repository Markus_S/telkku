package telkku.other;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Transaction;

import telkku.db.CloseableSession;
import telkku.db.HibernateUtil;

import com.opensymphony.xwork2.ActionSupport;

public class ParseModel extends ActionSupport {
	private static final Logger logger = Logger.getLogger(ParseModel.class);
	private static final long serialVersionUID = -397236277079501859L;
	private final Parser parser;
	private static Map<String, Channel> channelMap = new HashMap<String, Channel>();
	private static Map<String, String> excludedChannels = new HashMap<String, String>();

	public ParseModel() {
		this(new Telvis_fi());

		try (InputStream is = Telvis_fi.class.getResourceAsStream("Exclude_channel.xml")) {
			Properties prop = new Properties();
			prop.loadFromXML(is);
			Set<Object> keys = prop.keySet();

			for (Object key : keys) {
				excludedChannels.put((String) key, (String) prop.get(key));
			}
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	public ParseModel(Parser parser) {
		this.parser = parser;

		if (channelMap.size() == 0) {
			getChannelList();
		}
	}

	public void parsePrograms(String channel, Date day) {
		//TODO Mieti parempi ratkaisu tähän! (tuhlaa resursseja kutsua joka kerta)
		if (!excludedChannels.containsValue(channel)) {
			List<Program> programs = parser.parsePrograms(channel, day);

			if (programs.size() > 0) {
				try (CloseableSession session = new CloseableSession(HibernateUtil.getSessionFactory().getCurrentSession())) {
					Transaction tx = session.delegate().beginTransaction();
					Channel tmpChannel = channelMap.get(channel);

					for (Program program : programs) {
						program.setChannel(tmpChannel);
						session.delegate().save(program);
					}

					tx.commit();
				}
			}
		}
	}

	@SuppressWarnings("unchecked")
	private void getChannelList() {
		List<Channel> channelList = new ArrayList<>();

		try (CloseableSession session = new CloseableSession(HibernateUtil.getSessionFactory().getCurrentSession())) {
			Transaction tx = session.delegate().beginTransaction();
			channelList = session.delegate().createQuery("FROM Channel").list();
			tx.commit();
		} catch (HibernateException e) {
			logger.debug(e.getMessage());
			addActionMessage(e.getMessage());
		}

		for (Channel channel : channelList) {
			channelMap.put(channel.getChannelName(), channel);
		}
	}

	public boolean getError() {
		return parser.getError();
	}
}
