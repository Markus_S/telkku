package telkku.other;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import telkku.db.HibernateUtil;

public class HibernateListener implements ServletContextListener {

	@Override
	public void contextInitialized(ServletContextEvent event) {
		HibernateUtil.getSessionFactory(); // Just call the static initializer
											// of that class
	}

	@Override
	public void contextDestroyed(ServletContextEvent event) {
		HibernateUtil.getSessionFactory().close(); // Free all resources
	}

}
