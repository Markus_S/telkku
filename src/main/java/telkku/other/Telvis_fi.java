package telkku.other;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Entities;
import org.jsoup.select.Elements;

import com.opensymphony.xwork2.ActionSupport;

public class Telvis_fi extends ActionSupport implements Parser {
	private static final long serialVersionUID = -397336277079401859L;
	private static final Logger logger = Logger.getLogger(Telvis_fi.class);
	private static Map<String, String> channels = new HashMap<String, String>();
	private final static String BASEURL = "http://www.telvis.fi/tvohjelmat/";
	private final static String parameters = "?vw=channel&sh=all";
	private final static DateFormat df = new SimpleDateFormat("dd.MM.yyyy");
	private final static String charset = "iso-8859-1";
	private static boolean error;

	public Telvis_fi() {
		try (InputStream is = Telvis_fi.class.getResourceAsStream("Telvis_fi.xml")) {
			Properties prop = new Properties();
			prop.loadFromXML(is);
			Set<Object> keys = prop.keySet();

			for (Object key : keys) {
				channels.put((String) key, (String) prop.get(key));
			}
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	@Override
	public List<Program> parsePrograms(String channel, Date day) {
		List<Program> programList = new ArrayList<Program>();
		error = false;

		try {
			logger.debug("Parsing programs for channel: " + channel + " and date: " + df.format(day));
			URL url = getUrl(channel, day);
			Document doc = Jsoup.parse(url.openStream(), charset, url.toString());

			// Adjust escape mode
			doc.outputSettings().escapeMode(Entities.EscapeMode.xhtml);

			Elements programs = doc.select("div.program");

			if (programs.isEmpty()) {
				logger.debug("No program information for " + df.format(day));
				// System.out.println("No program information for " + channel +
				// " " + df.format(day));
			} else {

				for (Element program : programs) {
					Elements programInformation;
					if ((programInformation = program.select("div.chndesc")).isEmpty()) {
						// Picture in program info, get info from elsewhere.
						programInformation = program.select("div.chnsplitted");
					}

					programInformation.select("span.iconset").remove();
					String startTime = programInformation.select("span.time").html();
					programInformation.select("h4").select("span.time").remove();
					programInformation.select("h4").select("img").remove();
					String name = StringEscapeUtils.unescapeHtml4(programInformation.select("h4").html().replace("&apos;", "'"));
					programInformation.select("h4").remove();
					String desc = StringEscapeUtils.unescapeHtml4(programInformation.html().replace("&apos;", "'"));

					Program tmpProgram = new Program();
					tmpProgram.setProgramName(name);
					tmpProgram.setProgramDescription(desc);

					Calendar tmpDate = prepareStarttime(day, startTime);
					tmpProgram.setProgramStartTime(tmpDate.getTime());

					programList.add(tmpProgram);
				}
			}
		} catch (IOException e) {
			addActionError(e.getLocalizedMessage());
			error = true;
		}

		return programList;
	}

	/**
	 * @param channel
	 * @param day
	 * @return the url
	 */
	private URL getUrl(String channel, Date day) {
		URL url = null;

		try {
			url = new URL(BASEURL + parameters + "&ch=" + channels.get(channel) + "&dy=" + df.format(day));
		} catch (MalformedURLException ex) {
			logger.debug(ex.getMessage());
			addActionError(ex.getLocalizedMessage());
		}

		return url;
	}

	private Calendar prepareStarttime(Date day, String startTime) {
		Calendar tmpDate = new GregorianCalendar();

		int startTimeHours = Integer.parseInt(startTime.substring(0, 2));
		int startTimeMinutes = Integer.parseInt(startTime.substring(3));
		int startTimeSeconds = 0;
		int startTimeMilliSeconds = 0;

		// If start time is before 5 am => change to next day
		if (startTimeHours < 5) {
			tmpDate.setTime(Helper.addDaysToDate(day, 1));
		} else {
			tmpDate.setTime(day);
		}

		tmpDate.set(Calendar.HOUR_OF_DAY, startTimeHours);
		tmpDate.set(Calendar.MINUTE, startTimeMinutes);
		tmpDate.set(Calendar.SECOND, startTimeSeconds);
		tmpDate.set(Calendar.MILLISECOND, startTimeMilliSeconds);

		return tmpDate;
	}

	@Override
	public boolean getError() {
		return error;
	}
}
