package telkku.other;

import java.util.Date;
import java.util.List;

public interface Parser {
	public List<Program> parsePrograms(String channel, Date day);
	public boolean getError();
}