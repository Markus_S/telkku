package telkku.other;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Calendar;
import java.util.Date;

/**
 * @author Macabre
 */
public class Helper {
	/** yyyy-MM-dd **/
	public static final SimpleDateFormat df1 = new SimpleDateFormat("yyyy-MM-dd");
	/** yyyy-MM-dd HH:mm **/
	public static final SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd HH:mm");

	/**
	 * @param date
	 *            Date on which we want to add or decrease days
	 * @param amount
	 *            Amount that we want to add or decrease.
	 * @return new date which has been added or decreased days
	 */
	public static Date addDaysToDate(Date date, int amount) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.DATE, amount);
		return c.getTime();
	}

	/**
	 * @param date
	 *            Date on which we want to add or decrease hours
	 * @param amount
	 *            Amount that we want to add or decrease.
	 * @return new date which has been added or decreased hours
	 */
	public static Date addHoursToDate(Date date, int amount) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.HOUR, amount);
		return c.getTime();
	}

	public static boolean isTodayAfter(String date) {
		boolean result = false;
		Date today = Calendar.getInstance().getTime();

		try {
			result = today.after(df1.parse(date));
			System.out.println(result);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		return result;
	}

	/**
	 * This method return date without time. So time will be 00:00.
	 *
	 * @param date
	 *            Date on which we want to strip time off.
	 * @return date without time
	 */
	public static Date stripTime(Date date) {
		try {
			date = df1.parse(df1.format(date));
		} catch (ParseException e) {
			System.out.println("Parse error:" + e.getLocalizedMessage());
		}

		return date;
	}
	
	/**
	 * This method returns the maintenance starting datetime, which should be next day at 5 AM.
	 * 
	 * @return maintenance starting datetime
	 */
	public static LocalDateTime getMaintenanceStartDate() {
		LocalDate date = LocalDate.now().plusDays(1);
		LocalTime time = LocalTime.of(5, 0);
		LocalDateTime startDateTime = LocalDateTime.of(date, time);
		
		return startDateTime;
	}
}
