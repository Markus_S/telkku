package telkku.other;

import java.util.Comparator;

public class ProgramTimeComparator implements Comparator<Program>
{
  @Override
  public int compare(Program o1, Program o2)
  {
    return o1.getProgramStartTime().compareTo(o2.getProgramStartTime());
  }
}
