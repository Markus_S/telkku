package telkku.other;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

import org.hibernate.annotations.ForeignKey;
//import org.hibernate.annotations.Index;
import org.hibernate.search.annotations.DateBridge;
import org.hibernate.search.annotations.DocumentId;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.Resolution;

@Entity
@Indexed
//@org.hibernate.annotations.Table(appliesTo = "Program", indexes = { @Index(name = "channelTimeIndex", columnNames = { "channelID", "programStartTime" }),
//		@Index(name = "timeNameIndex", columnNames = { "programStartTime", "programName" }) })
@Table(name = "Program", indexes = { @javax.persistence.Index(name = "channelTimeIndex", columnList = "channelID, programStartTime"),
		@javax.persistence.Index(name = "timeNameIndex", columnList = "programStartTime, programName") })
public class Program {
	@Id
	@DocumentId
	@GeneratedValue
	private Long programId;
	@ManyToOne
	@JoinColumn(name = "channelId")
	@ForeignKey(name = "channelIndex")
	private Channel channel;
	@Field
	private String programName;
	@Lob
	@Field
	private String programDescription;
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	@Field
	@DateBridge(resolution = Resolution.MINUTE)
	private Date programStartTime;

	public Program() {
	}

	public Long getProgramId() {
		return programId;
	}

	public void setProgramId(Long programId) {
		this.programId = programId;
	}

	public Channel getChannel() {
		return channel;
	}

	public void setChannel(Channel channel) {
		this.channel = channel;
	}

	public String getProgramName() {
		return programName;
	}

	public void setProgramName(String programName) {
		this.programName = programName;
	}

	public String getProgramDescription() {
		return programDescription;
	}

	public void setProgramDescription(String programDescription) {
		this.programDescription = programDescription;
	}

	public Date getProgramStartTime() {
		return programStartTime;
	}

	public void setProgramStartTime(Date programStartTime) {
		this.programStartTime = programStartTime;
	}

	@Override
	public String toString() {
		return "Program [programId=" + programId + ", channel=" + channel + ", programName=" + programName + ", programDescription=" + programDescription
				+ ", programStartTime=" + programStartTime + "]";
	}
}
