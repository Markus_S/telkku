package telkku.other;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.Index;

@Entity
@Table
public class FavouriteProgram {
	
	@Id
	@GeneratedValue
	private Long favouriteProgramId;
	@Index(name = "favouriteIndex")
	private String favouriteProgramName;

	public FavouriteProgram(){	}

	public Long getFavouriteProgramId() {
		return favouriteProgramId;
	}

	public void setFavouriteProgramId(Long favouriteProgramId) {
		this.favouriteProgramId = favouriteProgramId;
	}

	public String getFavouriteProgramName() {
		return favouriteProgramName;
	}

	public void setFavouriteProgramName(String favouriteProgramName) {
		this.favouriteProgramName = favouriteProgramName;
	}

	@Override
	public String toString() {
		return "FavouriteProgram [favouriteProgramId=" + favouriteProgramId
				+ ", favouriteProgramName=" + favouriteProgramName + "]";
	}
}
