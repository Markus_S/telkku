import java.util.Date;

import telkku.other.ParseModel;
import telkku.other.Telvis_fi;

public class Test {
	public static void main(String[] args) {
		ParseModel pm = new ParseModel(new Telvis_fi());
		String channelName[] = { "TV1", "TV2", "MTV3", "Nelonen", "Sub", "TV Viisi", "YleTeema", "FST5", "Liv", "Jim", "Kutonen", "MTV3 AVA",
				"DEUTSCHE WELLE TV", "Eurosport HD", "FOX", "National Geographic", "Star", "TV5 Monde", "Travel Channel" };
		for (String channel : channelName) {
			pm.parsePrograms(channel, new Date());
		}
	}
}
