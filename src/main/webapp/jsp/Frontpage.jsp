<%@ include file="/WEB-INF/init.jsp"%>
<h1>Etusivu</h1>
<s:actionerror />
<s:actionmessage />

<div id="favouriteDiv">
	<h5>L�ytyneet suosikkiohjelmat seuraavan viikon ajalta:</h5>
	<ul id="favouriteList">
		<s:iterator value="favouritePrograms" var="program">
			<li>
				<div>
					<strong><s:property value="#program.programName" /></strong><br />
					<s:date name="#program.programStartTime" format="dd.MM HH:mm" />
					<s:property value="#program.channel.channelName" /><br />
					<dfn><s:property value="#program.programDescription" /></dfn>
				</div>
			</li>
		</s:iterator>
	</ul>
</div>
<div id="movieDiv">
	<h5>L�ytyneet elokuvat seuraavan viikon ajalta:</h5>
	<ul id="movieList">
		<s:iterator value="movies" var="program">
			<li>
				<div>
					<strong><s:property value="#program.programName" /></strong><br />
					<s:date name="#program.programStartTime" format="dd.MM HH:mm" />
					<s:property value="#program.channel.channelName" /><br />
					<dfn><s:property value="#program.programDescription" /></dfn>
				</div>
			</li>
		</s:iterator>
	</ul>
</div>

<s:debug />