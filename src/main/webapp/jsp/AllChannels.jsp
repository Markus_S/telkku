<%@ include file="/WEB-INF/init.jsp"%>
<h1>Ohjelmat</h1>
<h2><s:property value="date" /></h2>
<s:actionerror />
<s:actionmessage />

<s:form action="AllChannels" id="dateForm">
	<s:hidden id="date" name="date" />
	<%-- <s:submit /> --%>
</s:form>

<div id="programs">
	<table id="programList">
		<thead>
			<tr>
			<s:iterator value="channelListFiltered" var="channel">
				<th>
					<img alt="<s:property value="#channel.channelName" />" 
					title="<s:property value="#channel.channelName" />"  
					src='<s:property value="#channel.channelLogo" />'>
				</th>
			</s:iterator>
			</tr>
		</thead>
		<tbody>
			<s:iterator value="blockList" var="block">
				<tr>
					<s:iterator value="#block" var="blockPrograms">
						<td class="programBlock" style="vertical-align:top;">
							<s:iterator value="#blockPrograms" var="blockProgram">
								<div class="program">
									<div class="time"><em><s:date name="#blockProgram.programStartTime" format="HH:mm" /></em></div>
									<s:if test="%{#blockProgram.programId in favouriteProgramIds}">
										<s:set var="favourite" scope="page">favourite</s:set>
									</s:if>
									<s:else>
										<s:set var="favourite" scope="page" value="" />
									</s:else>
									<div class="name ${favourite}">
										<strong>
										<a href="#" onclick="toggleVisibility('desc_${blockProgram.programId}'); return false;">
											<s:property value="#blockProgram.programName" />
										</a>
										</strong>
									</div><br />
									<s:if test="#session.descriptions">
										<s:set var="visibility" scope="page">block</s:set>
									</s:if>
									<s:else>
										<s:set var="visibility" scope="page">none</s:set>
									</s:else>
									<div class="desc" id="desc_${blockProgram.programId}" style="display: ${visibility};">
										<s:property value="#blockProgram.programDescription" /><br />
									</div>
								</div>
							</s:iterator>
						</td>
					</s:iterator>
				</tr>
			</s:iterator>
		</tbody>
	</table>
</div>

<script>
	$(function() {
		$.datepicker.setDefaults( $.datepicker.regional[ "fi" ] );
		$( "#datepicker" ).datepicker({ 
			altField: "#date",
			dateFormat: 'yy-mm-dd',
			onSelect: function(dateText, inst) {
				document.dateForm.submit();
			}
		});
		$("#datepicker").datepicker("setDate",'<s:property value="date" />');
		$("div.program:even").addClass("stripe1");
		$("div.program:odd").addClass("stripe2");
		$("#programList div.program").hover(
			function() {
				$(this).toggleClass("highlight");
			},
			function() {
				$(this).toggleClass("highlight");
			}
		);
	});
	function toggleVisibility(id) {
		var e = document.getElementById(id);
		if (e.style.display == 'none') {
			e.style.display = 'block';
		} else {
			e.style.display = 'none';
		}
	}
</script>

<s:debug />