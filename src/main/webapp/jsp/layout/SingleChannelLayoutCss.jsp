<%@ include file="/WEB-INF/init.jsp"%>
<!DOCTYPE html>
<html lang="fi">
	<head>
		<!--<meta charset="UTF-8">-->
		<title><tiles:getAsString name="title" /></title>
		<link rel="stylesheet" href="css/programs.css" />
		<link rel="stylesheet" href="css/jquery-ui-1.11.2.css" />
		<link rel="icon" type="image/x-icon" href="images/favicon.ico">
		<script src="js/jquery-2.1.1.js"></script>
		<script src="js/jquery-ui-1.11.2.js"></script>
		<script src="js/jquery-ui-i18n.js"></script>
		<s:head/>
	</head>
	<body>
		<div id="outer">
			<div id="header">
				<h1><s:a action="Frontpage">Telkku</s:a></h1>
				<h2>by Markus Sairanen</h2>
			</div>
			<div id="content">
				<div id="primaryContentContainer">
					<div id="primaryContent">
						<tiles:insertAttribute name="body" />
					</div>
				</div>
				<div id="secondaryContent">
					<h3>Navigointi</h3>
					<ul>
						<li><s:a href="AllChannels">Kaikki kanavat</s:a></li>
						<li><s:a href="SingleChannel">Yksitt�inen kanava</s:a></li>
					</ul>
					<div id="datepicker"></div>
					<div>
						<br />
						<h5>N�yt� kanava:</h5>
						<s:form id="channelForm" theme="simple">
							<s:iterator value="@telkku.action.TelkkuAction@getChannelList()" var="channelItr">
								<div class="checkboxes">
									<s:if test="channel.channelId == #channelItr.channelId">
										<s:set var="checked" scope="page">checked="checked"</s:set>
									</s:if>
									<s:else>
										<s:set var="checked" scope="page" value="" />
									</s:else>
									<input type="radio" name="singleChannel" id="${channelItr.channelId}" value="${channelItr.channelName}" ${checked} onclick="document.channelForm.submit();"> 
									<label for="${channelItr.channelId}"><img src='<s:property value="#channelItr.channelLogo" />'></label>
								</div>
								<br />
							</s:iterator>
							<%-- <s:submit theme="simple" /> --%>
						</s:form>
					</div>
				</div>
				<div class="clear"></div>
			</div>
			<div id="footer">
				<p>Copyright &copy; 2013 Designed by <a href="#">JMS Production</a></p>
			</div>
		</div>
	</body>
</html>