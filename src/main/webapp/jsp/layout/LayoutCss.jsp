<%@ include file="/WEB-INF/init.jsp"%>
<!DOCTYPE html>
<html lang="fi">
	<head>
		<!--<meta charset="UTF-8">-->
		<title><tiles:getAsString name="title" /></title>
		<link rel="stylesheet" href="css/default.css" />
		<link rel="icon" type="image/x-icon" href="images/favicon.ico">
		<script src="js/jquery-2.1.1.js"></script>
		<script src="js/jquery-ui-1.11.2.js"></script>
		<s:head/>
	</head>
	<body>
		<div id="outer">
			<div id="header">
				<h1><s:a action="Frontpage">Telkku</s:a></h1>
				<h2>by Markus Sairanen</h2>
			</div>
			<div id="menu"></div>
			<div id="content">
				<div id="primaryContentContainer">
					<div id="primaryContent">
						<tiles:insertAttribute name="body" />
					</div>
				</div>
				<div id="secondaryContent">
					<h3>Navigointi</h3>
					<ul>
						<li><s:a href="AllChannels">Kaikki kanavat</s:a></li>
						<li><s:a href="SingleChannel">Yksittäinen kanava</s:a></li>
						<li><s:a href="Search">Etsi ohjelmia</s:a></li>
					</ul>
				</div>
				<div class="clear"></div>
			</div>
			<div id="footer">
				<p>Copyright &copy; 2013-2014 Designed by <a href="#">JMS Production</a></p>
			</div>
		</div>
	</body>
</html>