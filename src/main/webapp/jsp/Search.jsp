<%@ include file="/WEB-INF/init.jsp"%>
<h1>Etsi ohjelmia</h1>
<s:actionerror />
<s:actionmessage />

<s:form action="Search">
	<s:label>Kirjoita hakusana(t), jo(i)lla haetaan ohjelman <strong>nimest�</strong>. K�yt� jokerimerkkin�: %</s:label>
	<s:textfield name="searchString" label="Hakusana" />
	<s:submit value="Hae" />
</s:form>
<br />
<h5>L�ytyneet ohjelmat seuraavan viikon ajalta:</h5>
<ul id="searchList">
	<s:iterator value="programList" var="program">
		<li>
			<div>
				<strong><s:property value="#program.programName" /></strong><br />
				<s:date name="#program.programStartTime" format="dd.MM HH:mm" />
				<s:property value="#program.channel.channelName" />
				<br />
				<dfn>
					<s:property value="#program.programDescription" />
				</dfn>
			</div>
		</li>
	</s:iterator>
</ul>

<s:debug />