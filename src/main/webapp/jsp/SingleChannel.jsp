<%@ include file="/WEB-INF/init.jsp"%>
<h1>Ohjelmat</h1>
<h2><s:property value="date" /></h2>
<s:actionerror />
<s:actionmessage />

<s:form action="SingleChannel" id="dateForm">
	<s:hidden id="date" name="date" />
	<%-- <s:submit /> --%>
</s:form>

<div id="programs">
	<table id="programList">
		<thead>
			<tr>
				<th>
					<img alt="<s:property value="channel.channelName" />" 
					title="<s:property value="channel.channelName" />"  
					src='<s:property value="channel.channelLogo" />'>
				</th>
			</tr>
		</thead>
		<tbody>
			<s:iterator value="blockList" var="block">
				<tr>
					<s:iterator value="#block" var="blockPrograms">
						<td style="vertical-align:top;">
							<s:iterator value="#blockPrograms" var="blockProgram">
								<div class="program">
									<div class="time"><em><s:date name="#blockProgram.programStartTime" format="HH:mm" /></em></div>
									<s:if test="%{#blockProgram.programId in favouriteProgramIds}">
										<s:set var="favourite" scope="page">favourite</s:set>
									</s:if>
									<s:else>
										<s:set var="favourite" scope="page" value="" />
									</s:else>
									<div class="${favourite}">
										<strong>
											<s:property value="#blockProgram.programName" />
										</strong>
									</div>
									<div class="desc" id="desc_${blockProgram.programId}">
										<s:property value="#blockProgram.programDescription" /><br />
									</div>
								</div>
							</s:iterator>
						</td>
					</s:iterator>
				</tr>
			</s:iterator>
		</tbody>
	</table>
</div>

<script>
	$(function() {
		$.datepicker.setDefaults( $.datepicker.regional[ "fi" ] );
		$( "#datepicker" ).datepicker({ 
			altField: "#date",
			dateFormat: 'yy-mm-dd',
			onSelect: function(dateText, inst) {
				document.dateForm.submit();
			}
		});
		$("#datepicker").datepicker("setDate",'<s:property value="date" />');
		$("#programList div.program:even").addClass("stripe1");
		$("#programList div.program:odd").addClass("stripe2");
		$("#programList div.program").hover(
			function() {
				$(this).toggleClass("highlight");
			},
			function() {
				$(this).toggleClass("highlight");
			}
		);
	});
</script>

<s:debug />