package telkku.other;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;
import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class ParseTest {
	private static ParseModel pm = new ParseModel();
	private String param;
	private boolean result;
	
	@Parameters
	public static Collection<Object[]> data() {
		return Arrays.asList(new Object[][] {
				{"TV1", false},
				{"TV2", false},
				{"MTV3", false},
				{"Nelonen", false},
				{"Sub", false},
				{"TV Viisi", false},
				{"YleTeema", false},
				{"FST5", false},
				{"Liv", false},
				{"Jim", false},
				{"Kutonen", false},
				{"MTV3 AVA", false},
				{"DEUTSCHE WELLE TV", false},
				{"Eurosport HD", false},
				{"FOX", false},
				{"National Geographic", false},
				{"Star", false},
				{"TV5 Monde", false},
				{"Travel Channel", false}
		});
	}
	
	public ParseTest(String param, boolean result) {
		this.param = param;
        this.result = result;
	}

	@Test
	public void test() {
		pm.parsePrograms(param, new Date());
		assertEquals("Error for channel: " + param, result, pm.getError());
	}

}
