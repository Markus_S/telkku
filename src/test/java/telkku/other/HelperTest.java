package telkku.other;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class HelperTest {
	private String param;
	private boolean result;
	
	@Parameters
	public static Collection<Object[]> data() {
		return Arrays.asList(new Object[][] {
				{"2014-09-29", true},
				{"2014-09-30", false},
				{"2014-09-28", true},
				{"2014-08-29", true},
				{"2014-10-29", false},
				{"2014-12-29", false}
		});
	}
	
	public HelperTest(String param, boolean result) {
		this.param = param;
        this.result = result;
	}

	@Test
	public void test() {
		assertEquals("Error for date: " + param, result, Helper.isTodayAfter(param));
	}

}
